import os
import json
import twitter
import urllib.request
from time import sleep
from mastodon import Mastodon
from derpibooru import Search, sort

# Current dir and init values
THIS_DIR = os.path.dirname(os.path.abspath(__file__))
pastPosts = []
pastMastoPosts = []
t = None
m = None
config = {
    'derpi': '',
    'search': '',
    'artist': False,
    'twitter': {
        'key': '',
        'secret': '',
        'token': '',
        'tokenSecret': ''
    },
    'mastodon': {
        'key': '',
        'secret': '',
        'token': '',
        'host': ''
    }
}


# Write past ids
def wPast():
    global THIS_DIR, pastPosts, pastMastoPosts

    jo = json.dumps(pastPosts[-200:])  # Save last 200 elements
    with open(os.path.join(THIS_DIR, 'past.json'), 'w') as f:
        f.write(jo)

    jo = json.dumps(pastMastoPosts[-200:])  # Save last 200 elements
    with open(os.path.join(THIS_DIR, 'pastMasto.json'), 'w') as f:
        f.write(jo)


# Send to Twitter
def postT(msg, image, id):
    global pastPosts, t
    if isinstance(t, str):
        print('String?? ' + t)
    else:
        # Post to Twitter
        t.PostUpdate(msg, media=image)
        # Add id to past ids
        pastPosts.append(id)
        # Log
        print('Tweeted #' + str(id))
        # Save
        wPast()

    # Wait a bit to prevent hard ratelimit
    sleep(3)


# Send to Mastodon
def postM(msg, image, id):
    global config, pastMastoPosts, m
    if m is None:
        return

    # Fetch media
    name = os.path.basename(image)
    try:
        with urllib.request.urlopen(image) as response, open(name, 'wb') as out_file:
            data = response.read()
            out_file.write(data)

            try:
                # Upload media
                media = m.media_post(name)
                if 'error' in media:
                    raise Exception(media['error'])

                # Post toot
                toot = m.status_post(msg, media_ids=[media])
                if 'error' in toot:
                    raise Exception(toot['error'])
                # Log
                pastMastoPosts.append(id)
                print('Tooted #' + str(id))
                # Save
                wPast()
            except:
                err(id, 'Fail on file upload', 'mastodon')
                pass

            # Delete local file
            os.remove(name)
    except:
        err(id, 'Fail on file download/delete', 'mastodon')
        pass


# Display error message
def err(id, err, size):
    print('Error #' + str(id) + ' on ' + size + ': ' + str(err))


# Create config file if it doesn't exists
if not os.path.isfile(os.path.join(THIS_DIR, 'config.json')):
    with open(os.path.join(THIS_DIR, 'config.json'), 'w') as f:
        f.write(json.dumps(config, indent=2))

    print('A config.json file has been created.')
    print('Please fill the values before running again.')
    exit(2)

# Load config file
with open(os.path.join(THIS_DIR, 'config.json')) as d:
    config = json.load(d)

# Check that all values in config are filled
if (config['derpi'] == '' or config['search'] == '' or
   config['twitter']['key'] == '' or config['twitter']['secret'] == '' or
   config['twitter']['token'] == '' or config['twitter']['tokenSecret'] == ''):
    print('Please complete your config.json file')
    exit(3)

# Stop if there's a lock file or create it
if os.path.isfile(os.path.join(THIS_DIR, 'derpi.lock')):
    print('locked')
    exit(0)
else:
    open(os.path.join(THIS_DIR, 'derpi.lock'), 'a').close()

# Create past ids file if it doesn't exists
if not os.path.isfile(os.path.join(THIS_DIR, 'past.json')):
    with open(os.path.join(THIS_DIR, 'past.json'), 'w') as f:
        f.write(json.dumps(pastPosts))

if not os.path.isfile(os.path.join(THIS_DIR, 'pastMasto.json')):
    with open(os.path.join(THIS_DIR, 'pastMasto.json'), 'w') as f:
        f.write(json.dumps(pastMastoPosts))

# Load past ids file
with open(os.path.join(THIS_DIR, 'past.json')) as d:
    pastPosts = json.load(d)

with open(os.path.join(THIS_DIR, 'pastMasto.json')) as d:
    pastMastoPosts = json.load(d)

# Login to Twitter API
t = twitter.Api(consumer_key=config['twitter']['key'],
                consumer_secret=config['twitter']['secret'],
                access_token_key=config['twitter']['token'],
                access_token_secret=config['twitter']['tokenSecret'])

# Use Mastodon if applicable
if (config['mastodon']['host'] != '' and config['mastodon']['key'] != '' and
   config['mastodon']['secret'] != '' and config['mastodon']['token'] != ''):
    m = Mastodon(
        client_id=config['mastodon']['key'],
        client_secret=config['mastodon']['secret'],
        access_token=config['mastodon']['token'],
        api_base_url=config['mastodon']['host'],
        ratelimit_method='pace'
    )


# Looks for our watched list posts
for post in Search().key(config['derpi']).sort_by(sort.CREATED_AT).limit(25).query(config['search']):
    # We want post who are properly rendered by derpi
    # If the created_at and first_seen_at are equal, that means it's not a duplicate/edit
    # And we check if we didn't post it already on birdsite and mastodon
    if (post.is_rendered and post.created_at == post.first_seen_at and (post.id not in pastPosts or post.id not in pastMastoPosts)):
        # print(post.id)
        # pastMastoPosts.append(post.id)
        # pastPosts.append(post.id)
        # wPast()
        # continue

        artists = ''
        if config['artist']:
            a_tags = [t for t in post.tags if t.startswith('artist:')]
            if len(a_tags) == 1:
                artists = '(artist: %s)' % a_tags[0].replace('artist:', '')
            elif len(a_tags) > 1:
                artists = '(artists: '

                for a in a_tags:
                    # Make sure we don't reach the max tweet length
                    # because there's 200 artists
                    if len(artists) < 200:
                        artists += a.replace('artist:', '') + ', '
                    else:
                        artists += '.....'  # Don't worry, two dots will disappear
                        break

                artists = artists[:-2] + ')'

        # Make text messagea
        msg = '#{} {}\n{}'.format(post.id, artists, post.url)

        # Add media - if it's a webm, we will need the mp4 version of it
        mediaURL = post.image
        if post.original_format == 'webm':
            mediaURL = post.representations['mp4']

        if post.id not in pastMastoPosts:
            print('M=> #' + str(post.id))
            postM(msg, mediaURL, post.id)

        if post.id in pastPosts:
            continue

        try:
            print('T=> #' + str(post.id))
            postT(msg, mediaURL, post.id)
        except twitter.error.TwitterError as e:
            if 'Images must be less than 5MB.' in str(e):
                # Try a smaller picture
                sizes = ['tall', 'small', 'thumb']
                for s in sizes:
                    try:
                        postT(msg, getattr(post, s), post.id)
                        break
                    except twitter.error.TwitterError as e:
                        if 'Images must be less than 5MB.' not in str(e):
                            err(post.id, e, s)
                            break
            else:
                err(post.id, e, 'full')

# Delete lock
os.remove(os.path.join(THIS_DIR, 'derpi.lock'))
