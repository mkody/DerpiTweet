# DerpiTweet
> Post Derpibooru search results to Twitter.

Example with [@DerpiUpGalore](https://twitter.com/DerpiUpGalore).


## Before using it
- python3
    - `pip3 install python-twitter`
    - `pip3 install Mastodon.py`
    - `pip3 install git+https://github.com/joshua-stone/DerPyBooru.git`
- Run `python3 derpi.py` once
- Edit `config.json` with
    - `derpi` with your Derpibooru API key (You can find it in bold here: <https://derpibooru.org/users/edit>)
    - `search` with your search query
    - `twitter[...]` with your Twitter app keys and token (create your app here: <https://apps.twitter.com/>)
    - `mastodon[...]` with your Mastodon app keys and token (leave everything empty if you don't care)


## Using it
Run the script, `python3 derpi.py`.

Here's a cron example:
```
*/1 * * * * python3 /path/to/derpi.py >> /path/to/cron.log 2>&1
```
_You might want to make sure that cron.log file get rotated/cleaned._

